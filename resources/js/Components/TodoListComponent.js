import React from 'react';

class TodoList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var items = this.props.items.map(todo => {
        return (<li key={todo.id}>{todo.text}</li>);
        });

        return (<ul>{items}</ul>);
    }
}

export default TodoList;
