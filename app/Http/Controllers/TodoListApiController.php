<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\TodoList;
class TodoListApiController extends Controller
{
    function list() {
        return TodoList::all();
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'unique:todo_lists'],
            'description' => ['required']
         ]);
        // if($validator->fails()){
        //     return response()->json($validator->errors(), 400);
        // }
        $todolist = TodoList::create($request->all());
        return response()->json($todolist, 201);
    }
    public function show($id)
    {
        return TodoList::find($id);
    }
}
