import React from 'react';
import TodoFilter from './TodoFilterComponent';
import TodoList from './TodoListComponent';

class TodoApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: 'test',
            items: [
                {id: 1, text: 'todo list item 1'},
                {id: 2, text: 'todo list item 2'},
                {id: 3, text: 'todo list item 3'}
            ]
        };
    }

    handleFilter(filter) {
        this.setState({
            ...this.state,
            filter
        });
    }

    getToDo() {
        return this.state.items.filter((item) => {
		var filterText = this.state.filter;
            return !filterText || item.text.includes(filterText);
        })
    }

    render() {
        return(<div>
                This is to do app
                <TodoFilter filter={this.state.filter}
                    onFilter={(filter) => this.handleFilter(filter)}/>
                <TodoList items={this.getToDo()} />
            </div>);
    }
}

export default TodoApp;
