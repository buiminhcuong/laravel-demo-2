<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TodoList;


class TodoListController extends Controller
{
    function list() {
        return view("Todolist.list",
            ["todolists" => TodoList::all(),
            "myname" => "N02-K58"]);
    }

    function create() {
        return view("Todolist.create");
    }

    function createSubmit(Request $request) {
        $data = $request->validate([
            'title' => 'required|unique:todo_lists|max:255',
            'description' => 'required|max:2000',
        ]);
        $todoList = new \App\Models\TodoList($data);
        $todoList->save();
        return redirect()->route('todolists.list');
    }

    function edit($id) {
        return "$id";
    }
}
