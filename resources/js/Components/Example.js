import React from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './TodoAppComponent'

function Example() {
    return (
        <div className="container">
            <TodoApp />
        </div>
    );
}

export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
