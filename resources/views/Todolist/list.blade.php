@extends('layouts.app')

@section('title', 'Todo List')

@section('content')
<div class="links">
    @foreach($todolists as $todolist)
        <h1>{{$todolist->title}}</h1>
        <h2>{{$todolist->description}}</h2>
        <hr/>
    @endforeach
</div>
@endsection
