## Requirement
PHP ^7.3
Composer
NodeJs

## How to run

Clone and run following command from CommandLine:


```
    composer install
    npm install && npm run dev
    php artisan serve
```

In order to having scss and js files outputing on the fly, run:

```
    npm run watch
```

## Slides

https://bitbucket.org/buiminhcuong/chuyende_slides

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
