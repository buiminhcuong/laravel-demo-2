import React from 'react';

class TodoFilter extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(<div>
            Filter: <input type="text" value={this.props.filter}
                onChange={(e) => this.props.onFilter(e.target.value)} />
        </div>);
    }
}

export default TodoFilter;
