import React from 'react';

class TestReact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.content
        };
    }

    handleChange(e) {
        var newState = {
            value: e.target.value
        };

        this.setState(newState);
    }

    render() {
        return (<div>
                <input type='text' value={this.state.value}
                    onChange={(e) => this.handleChange(e)} />
                <h1>{this.state.value}</h1>
            </div>);
    }
}

export default TestReact;
